use std::error;
use std::fs;
use std::io::Seek;
use std::io::{self, BufRead};
use std::path::Path;
fn main() -> Result<(), Box<dyn error::Error>> {
  let (data, width) = read_diagnotic_file("./res/input")?;

  println!("--- Part One ---");
  let gamma = gamma(&data, width);
  let epsilon = epsilon(gamma, width);
  let power = gamma * epsilon;
  println!(
    "gamma: {g:0w$b} ({g})\n\
    epsilon: {e:0w$b} ({e})\n\
    power: {}\n",
    power,
    g = gamma,
    e = epsilon,
    w = width
  );

  println!("--- Part Two ---");
  let oxygen_generator_rating = oxygen_generator_rating(&data, width).unwrap();
  let co2_scrubber_rating = co2_scrubber_rating(&data, width).unwrap();
  let life_support_rating = oxygen_generator_rating * co2_scrubber_rating;
  println!(
    "oxygen generator rating: {o:0w$b} ({o})\n\
      CO2 scrubber rating: {c:0w$b} ({c})\n\
      life support rating: {}\n",
    life_support_rating,
    o = oxygen_generator_rating,
    c = co2_scrubber_rating,
    w = width
  );

  Ok(())
}

fn read_diagnotic_file<P: AsRef<Path>>(path: P) -> Result<(Vec<usize>, usize), io::Error> {
  let mut file = fs::File::open(path)?;
  let data = io::BufReader::new(&file)
    .lines()
    .filter_map(|maybe_line| {
      maybe_line
        .ok()
        .and_then(|line| usize::from_str_radix(&line, 2).ok())
    })
    .collect();
  file.seek(std::io::SeekFrom::Start(0))?;
  let width = io::BufReader::new(file)
    .lines()
    .take(1)
    .fold(0usize, |_width, line| line.unwrap().len());
  Ok((data, width))
}

fn high_bit_count(data: &[usize], position: usize) -> usize {
  data
    .iter()
    .filter(|&entry| ((entry >> position) & 1) > 0)
    .count()
}

/// according to the problem statement, we return 1 if the two states are equally populous
fn most_common_bit(data: &[usize], position: usize) -> usize {
  let highs = high_bit_count(data, position);
  let lows = data.len() - highs;
  (highs >= lows) as usize
}

fn gamma(data: &[usize], width: usize) -> usize {
  let mut gamma_parts = vec![0usize; width];
  gamma_parts
    .iter_mut()
    .enumerate()
    .for_each(|(position, bit)| {
      *bit = most_common_bit(data, position);
    });
  gamma_parts
    .iter()
    .enumerate()
    .fold(0usize, |out, (position, bit)| out | bit << position)
}

fn epsilon(gamma: usize, width: usize) -> usize {
  !(!0usize << width) & !gamma // this is nice
}

fn oxygen_generator_rating(data: &[usize], width: usize) -> Result<usize, ()> {
  select_via_bit_criteria(data, true, width)
}

fn co2_scrubber_rating(data: &[usize], width: usize) -> Result<usize, ()> {
  select_via_bit_criteria(data, false, width)
}

fn select_via_bit_criteria(data: &[usize], criteria: bool, width: usize) -> Result<usize, ()> {
  // if criteria is true, then the most common bit is selected,
  // if criteria is false, then the least common bit is selected.
  let mut filtered = Vec::from(data.clone());
  for position in (0..width).rev() {
    let select = (most_common_bit(&filtered, position) == criteria as usize) as usize;
    if filtered.len() > 1 {
      filtered = filtered
        .iter()
        .filter(|&entry| ((entry >> position) & 1) == select)
        .cloned()
        .collect();
    }
  }
  if filtered.len() == 1 {
    Ok(filtered.first().unwrap().clone())
  } else {
    println!("Something wrong happened\nfiltered: {:?}", filtered);
    Err(())
  }
}
